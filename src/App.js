import { Heading } from "@chakra-ui/react";
import ToDoList from "./components/ToDoList";
import Addtodo from "./components/AddTodo";
import { VStack } from "@chakra-ui/react";
import { IconButton } from "@chakra-ui/react";
import { FaSun, FaMoon } from "react-icons/fa";
import { useState, useEffect } from "react";
function App() {
  // const intialtodos = [
  //   {
  //     id: 1,
  //     body: "one",
  //   },
  //   {
  //     id: 2,
  //     body: "two",
  //   },
  //   {
  //     id: 3,
  //     body: "three",
  //   },
  // ];
  const [todos, setTodos] = useState([]);

  function Deletetodo(id) {
    const newtodos = todos.filter((todo) => todo.id !== id);
    setTodos(newtodos);
  }

  function addTodo(todo) {
    setTodos([...todos, todo]);
  }

  return (
    <VStack p={5} mb="10">
      <Heading
        mb="5"
        fontWeight="bold"
        size="2xl"
        bgGradient="linear(to-r, pink.500, pink.300, blue.500)"
        bgClip="text"
      >
        ToDo Application
      </Heading>
      <ToDoList todos={todos} Deletetodo={Deletetodo} />
      <Addtodo addTodo={addTodo} />
    </VStack>
  );
}

export default App;

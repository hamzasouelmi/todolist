import React from "react";
import {
  HStack,
  VStack,
  Text,
  IconButton,
  StackDivider,
  Spacer,
  Badge,
} from "@chakra-ui/react";
import { FaTrash } from "react-icons/fa";
import { FaEdit } from "react-icons/fa";

function ToDoList({ todos, Deletetodo }) {
  if (todos.length === 0) {
    return (
      <Badge variant="outline" colorScheme="blue" p="4" m="4">
        No TODOS FOR TODAY!!
      </Badge>
    );
  }

  return (
    <VStack divider={<StackDivider />} p={4} width="70%" alignItems={"strech"}>
      {todos.map((todo) => {
        return (
          <HStack key={todo.id}>
            <Text>{todo.body}</Text>
            <Spacer />
            <IconButton icon={<FaEdit />} isRound={true} />
            <IconButton
              icon={<FaTrash />}
              isRound={true}
              onClick={() => Deletetodo(todo.id)}
            />
          </HStack>
        );
      })}
    </VStack>
  );
}

export default ToDoList;

import React, { useState } from "react";
import { Button, Input, VStack, useToast } from "@chakra-ui/react";
import { nanoid } from "nanoid";

function Addtodo({ addTodo }) {
  const toast = useToast();
  function HandleSubmit(e) {
    e.preventDefault();

    if (!content) {
      toast({
        title: "Please add a task first",
        status: "warning",
        duration: 2000,
        isClosable: true,
      });
      return;
    }

    const todo = {
      id: nanoid(),
      body: content,
    };
    addTodo(todo);
    SetContent("");
  }
  const [content, SetContent] = useState("");

  return (
    <form onSubmit={HandleSubmit}>
      <VStack width="100%" alignItems={"strech"} mt="5">
        <Input
          placeholder="Enter your todo"
          variant="outline"
          value={content}
          onChange={(e) => SetContent(e.target.value)}
        />
        <Button colorScheme="green" px="5" type="submit">
          Add Todo
        </Button>
      </VStack>
    </form>
  );
}

export default Addtodo;
